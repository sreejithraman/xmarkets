package com.team.XMarkets;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.os.Parcelable;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class StoreInfo extends Activity{

	private Store store;
	
	private TextView storeName;
	private TextView storeAddress;
	private ListView promotionList;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.storeinfo);

		store = getIntent().getExtras().getParcelable("store");
		setWidgets();

	}
	
	private void setWidgets() {
		storeName = (TextView) findViewById(R.id.storeInfo_name);
		storeName.setText(store.getName());
		
		storeAddress = (TextView) findViewById(R.id.storeInfo_address);
		storeAddress.setText(store.getAddress());
		
		promotionList = (ListView) findViewById(R.id.storeInfo_promotions);
		List<Promotion> promotions = store.getCollectionPromotion();
		ArrayAdapter<Promotion> promotionListAdapter = new ArrayAdapter<Promotion>(this,
				android.R.layout.simple_list_item_1, 
				promotions);
		promotionList.setAdapter(promotionListAdapter);
	}

}