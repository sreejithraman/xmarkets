package com.team.XMarkets;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;


public class ShoppingList extends Activity {
	private Button setStore;
	private AutoCompleteTextView itemInput;
	private ListView itemList;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.shoppinglist);
		setWidgets();
	}

	private void setWidgets() {
		final ArrayAdapter<Item> itemListAdapter = new ArrayAdapter<Item>(this,
				android.R.layout.simple_list_item_1, 
				((Utilities)this.getApplication()).getUserItems());
		itemList = (ListView) findViewById(R.id.shoppingList);
		itemList.setAdapter(itemListAdapter);
		itemList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Item item = (Item)parent.getItemAtPosition(position);
				Intent intent = new Intent(ShoppingList.this, ItemInfo.class);
				intent.putExtra("item", item);
				startActivity(intent);
			}
		});

		itemInput = (AutoCompleteTextView) findViewById(R.id.inputShoppingItem);
		ArrayAdapter<Item> itemInputAdapter = new ArrayAdapter<Item>(this,
				android.R.layout.select_dialog_item, 
				((Utilities)this.getApplication()).getStoreItems());
		itemInput.setAdapter(itemInputAdapter);
		itemInput.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Item item = (Item)parent.getItemAtPosition(position);
				((Utilities)getApplication()).addUserItem(item);
				itemListAdapter.notifyDataSetChanged();
				itemInput.setText("");

			}
		});

		setStore = (Button) findViewById(R.id.setStore);
		setStore.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(ShoppingList.this, SetStore.class);
				startActivity(intent);
			}
		});

	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onResume()
	 */
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		ArrayAdapter<Item> itemInputAdapter = new ArrayAdapter<Item>(this,
				android.R.layout.select_dialog_item, 
				((Utilities)this.getApplication()).getStoreItems());
		itemInput.setAdapter(itemInputAdapter);
		if(((Utilities)getApplication()).getCurrentStore() != null)
		{
			setStore.setText(((Utilities)getApplication()).getCurrentStore().getName());
		}
	}
}
