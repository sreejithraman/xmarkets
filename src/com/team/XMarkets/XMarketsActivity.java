package com.team.XMarkets;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

public class XMarketsActivity extends TabActivity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.main);

	TabHost tabHost = getTabHost();
	
	//Shopping List tab
	Intent intentShoppingList = new Intent().setClass(this, ShoppingList.class);
	TabSpec tabSpecShoppingList = tabHost
	  .newTabSpec("Shopping List")
	  .setIndicator("Shopping List")
	  .setContent(intentShoppingList);
	
	//Store List tab
	Intent intentStoreList = new Intent().setClass(this, StoreList.class);
	TabSpec tabSpecStoreList = tabHost
	  .newTabSpec("Store List")
	  .setIndicator("Store List")
	  .setContent(intentStoreList);
	
	//add tabs
	tabHost.addTab(tabSpecShoppingList);
	tabHost.addTab(tabSpecStoreList);
	
    }
}
