package com.team.XMarkets;


import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class StoreList extends Activity {

	private EditText storeFilter;
	private ListView storeList;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.storelist);

		setWidgets();
	}

	private void setWidgets() {
		final ArrayAdapter<Store> storeListAdapter = new ArrayAdapter<Store>(this,
				android.R.layout.simple_list_item_1, 
				((Utilities)this.getApplication()).getLocalStores());
		storeList = (ListView) findViewById(R.id.storeList);
		storeList.setAdapter(storeListAdapter);
		storeList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				//            	Item item = (Item)parent.getItemAtPosition(position);
				Intent intent = new Intent(StoreList.this, StoreMap.class);
				//            	intent.putExtra("item", item);
				startActivity(intent);
			}
		});

		storeList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View v, int position, long id) {
				Store store = (Store)parent.getItemAtPosition(position);
				Intent intent = new Intent(StoreList.this, StoreInfo.class);
				intent.putExtra("store", store);
				startActivity(intent);
				return false;
			}
		}); 

		storeFilter = (EditText) findViewById(R.id.inputStore);
		storeFilter.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				// TODO Auto-generated method stub
				storeListAdapter.getFilter().filter(arg0);
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub

			}
			

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}
		});

	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onResume()
	 */
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		ArrayAdapter<Store> storeListAdapter = new ArrayAdapter<Store>(this,
				android.R.layout.simple_list_item_1, 
				((Utilities)this.getApplication()).getLocalStores());
		storeList = (ListView) findViewById(R.id.storeList);
		storeList.setAdapter(storeListAdapter);
		
	}
	
	
}
