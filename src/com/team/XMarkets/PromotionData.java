package com.team.XMarkets;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class PromotionData {
	// url to create new promotion
	private static String url_create_promotion = "http://192.168.1.18/xmarkets/create_promotion.php";
	private static final String url_update_promotion = "http://192.168.1.18/xmarkets/update_promotion.php";
	private static final String url_delete_promotion = "http://192.168.1.18/xmarkets/delete_promotion.php";
	private static String url_all_promotions = "http://192.168.1.18/xmarkets/get_all_promotions.php";

	private static final String TAG_SUCCESS = "success";
	private static final String TAG_PROMOTIONID = "promotionid";
	private static final String TAG_NAME = "name";
	private static final String TAG_DESCRIPTION = "description";
	private static final String TAG_STARTDATE = "startdate";
	private static final String TAG_ENDDATE = "enddate";
	private static final String TAG_STOREID = "storeid";
	private static final String TAG_PROMOTION = "promotion";
	private static final String TAG_CATEGORYID = "categoryid";

	public static int update(String name, String description, String start_date, String end_date,
			int promotionId, int storeId){

		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(TAG_PROMOTIONID, Integer.toString(promotionId)));
		params.add(new BasicNameValuePair(TAG_NAME, name));
		params.add(new BasicNameValuePair(TAG_DESCRIPTION, description));
		params.add(new BasicNameValuePair(TAG_STARTDATE, start_date));
		params.add(new BasicNameValuePair(TAG_ENDDATE, end_date));
		params.add(new BasicNameValuePair(TAG_STOREID, Integer.toString(storeId)));

		JSONParser jsonParser = new JSONParser();

		// sending modified data through http request
		// Notice that update promotion url accepts POST method
		JSONObject json = jsonParser.makeHttpRequest(url_update_promotion,
				"POST", params);

		// check json success tag
		try {
			int success = json.getInt(TAG_SUCCESS);

			if (success == 1) {
				return 1;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return 0;
	}

	public static int remove(int promotionId){
		JSONParser jsonParser = new JSONParser();

		int success;
		try {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("promotionid", Integer.toString(promotionId)));

			// getting promotion details by making HTTP request
			JSONObject json = jsonParser.makeHttpRequest(
					url_delete_promotion, "POST", params);

			// check your log for json response
			Log.d("Delete promotion", json.toString());

			// json success tag
			success = json.getInt(TAG_SUCCESS);
			if (success == 1) {
				return 1;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return 0;
	}

	public static ArrayList<Promotion> get(){
		
		JSONParser jParser = new JSONParser();
		JSONArray promotions = null;
		ArrayList<Promotion> promotionList = new ArrayList<Promotion>();

		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		// getting JSON string from URL

		JSONObject json = jParser.makeHttpRequest(url_all_promotions, "GET", params);

		// Check your log cat for JSON reponse
		Log.d("All promotions check: ", json.toString());

		try {
			// Checking for SUCCESS TAG
			int success = json.getInt(TAG_SUCCESS);

			if (success == 1) {
				// promotions found
				// Getting Array of promotions
				promotions = json.getJSONArray(TAG_PROMOTION);
				// looping through All promotions
				for (int i = 0; i < promotions.length(); i++) {
					JSONObject c = promotions.getJSONObject(i);
					// Storing each json promotion in variable
					String id = c.getString(TAG_PROMOTIONID);
					String name = c.getString(TAG_NAME);
					String description = c.getString(TAG_DESCRIPTION);
					String start_date = c.getString(TAG_STARTDATE);
					String end_date = c.getString(TAG_ENDDATE);
					String storeid = c.getString(TAG_STOREID);
					String catid = c.getString(TAG_CATEGORYID);
					Promotion temp = new Promotion(name, description, start_date, end_date, (int)Integer.parseInt(id), (int)Integer.parseInt(catid), (int)Integer.parseInt(storeid));
					promotionList.add(temp);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return promotionList;
	}
	
	public static int add(String name, String description, String start_date, String end_date, int catId, int storeId){

		JSONParser jsonParser = new JSONParser();

		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("name", name));
		params.add(new BasicNameValuePair("description", description));
		params.add(new BasicNameValuePair("startdate", start_date));
		params.add(new BasicNameValuePair("enddate", end_date));
		params.add(new BasicNameValuePair("categoryid", Integer.toString(catId)));
		params.add(new BasicNameValuePair("storeid", Integer.toString(storeId)));
		
		// getting JSON Object
		// Note that create promotion url accepts POST method
		JSONObject json = jsonParser.makeHttpRequest(url_create_promotion,
				"POST", params);

		// check log cat fro response
		Log.d("Create Response", json.toString());

		// check for success tag
		try {
			int success = json.getInt(TAG_SUCCESS);

			if (success == 1) {
				return 1;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return 0;
	}
}