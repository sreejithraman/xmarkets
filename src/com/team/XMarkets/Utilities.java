package com.team.XMarkets;

import java.util.ArrayList;

import org.w3c.dom.ls.LSOutput;

import android.app.Application;

public class Utilities extends Application {

	private ArrayList<Item> storeItems;
	private ArrayList<Item> userItems;//can add string or item (string for products that don't exist)
	private ArrayList<Store> localStores;
	private Store currentStore;

	/**
	 * @return the currentStore
	 */
	public Store getCurrentStore() {
		return currentStore;
	}

	/**
	 * @param currentStore the currentStore to set
	 */
	public void setCurrentStore(Store currentStore) {
		this.currentStore = currentStore;
	}

	public void onCreate()
	{
		super.onCreate();
		userItems = new ArrayList<Item>();
		storeItems = fetchStoreItems();
		localStores = fetchLocalStores();
	}

	public ArrayList<Item> fetchStoreItems() {
		ArrayList<Item> sItems = new ArrayList<Item>();
		sItems = ItemData.get();
		return sItems;
	}

	public ArrayList<Store> fetchLocalStores()
	{
		ArrayList<Store> lStores = new ArrayList<Store>();

		//temp---normally get the stuff from the database
		for(int i = 0; i < 10; i++)
		{
			ArrayList<Item> items = fetchStoreItems();
			items.remove(0);
			ArrayList<Promotion> promos = new ArrayList<Promotion>();
			promos = PromotionData.get();
			Store store = new Store("Store "+i, "218 Wiggins Street", "store@store.com", "7651234567", i, "user "+i, "pwd"+i, new ArrayList<Category>(), promos, items);
			lStores.add(store);
		}
		//___
		return lStores;
	}

	/**
	 * @return the localStores
	 */
	public ArrayList<Store> getLocalStores() {
		return localStores;
	}

	/**
	 * @param localStores the localStores to set
	 */
	public void setLocalStores(ArrayList<Store> localStores) {
		this.localStores = localStores;
	}

	public ArrayList<Item> getUserItems() {
		return userItems;
	}

	public void setUserItems(ArrayList<Item> userItems) {
		this.userItems = userItems;
	}

	public void addUserItem(Item item)
	{
		userItems.add(item);
	}

	public ArrayList<Item> getStoreItems() {
		return storeItems;
	}

	public void setStoreItems(ArrayList<Item> storeItems) {
		this.storeItems = storeItems;
	} 



}
