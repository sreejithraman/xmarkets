package com.team.XMarkets;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class ItemData {
	// url to create new product
	private static String url_create_item = "http://192.168.1.18/xmarkets/create_item.php";
	private static final String url_update_item = "http://192.168.1.18/xmarkets/update_item.php";
	private static final String url_delete_item = "http://192.168.1.18/xmarkets/delete_item.php";
	private static String url_all_items = "http://192.168.1.18/xmarkets/get_all_items.php";

	private static final String TAG_SUCCESS = "success";
	private static final String TAG_ITEMID = "itemid";
	private static final String TAG_NAME = "name";
	private static final String TAG_DESCRIPTION = "description";
	private static final String TAG_AISLE = "aisle";
	private static final String TAG_X = "x";
	private static final String TAG_Y = "y";
	private static final String TAG_PRICE = "price";
	private static final String TAG_QUANTITY = "quantity";
	private static final String TAG_CATEGORYID = "categoryid";
	private static final String TAG_STOREID = "storeid";
	private static final String TAG_ITEM = "item";

	public static int update(String name, String description, String aisle, int x, int y,
			double price, int quantity, int itemId, int categoryId, int storeId){

		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(TAG_ITEMID, Integer.toString(itemId)));
		params.add(new BasicNameValuePair(TAG_NAME, name));
		params.add(new BasicNameValuePair(TAG_DESCRIPTION, description));
		params.add(new BasicNameValuePair(TAG_AISLE, aisle));
		params.add(new BasicNameValuePair(TAG_X, Integer.toString(x)));
		params.add(new BasicNameValuePair(TAG_Y, Integer.toString(y)));
		params.add(new BasicNameValuePair(TAG_PRICE, Double.toString(price)));
		params.add(new BasicNameValuePair(TAG_QUANTITY, Integer.toString(quantity)));
		params.add(new BasicNameValuePair(TAG_CATEGORYID, Integer.toString(categoryId)));
		params.add(new BasicNameValuePair(TAG_STOREID, Integer.toString(storeId)));

		JSONParser jsonParser = new JSONParser();

		// sending modified data through http request
		// Notice that update product url accepts POST method
		JSONObject json = jsonParser.makeHttpRequest(url_update_item,
				"POST", params);

		// check json success tag
		try {
			int success = json.getInt(TAG_SUCCESS);

			if (success == 1) {
				return 1;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return 0;
	}

	public static int remove(int itemId){
		JSONParser jsonParser = new JSONParser();

		int success;
		try {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("itemid", Integer.toString(itemId)));

			// getting product details by making HTTP request
			JSONObject json = jsonParser.makeHttpRequest(
					url_delete_item, "POST", params);

			// check your log for json response
			Log.d("Delete Product", json.toString());

			// json success tag
			success = json.getInt(TAG_SUCCESS);
			if (success == 1) {
				return 1;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return 0;
	}

	public static ArrayList<Item> get(){
		
		JSONParser jParser = new JSONParser();
		JSONArray items = null;
		ArrayList<Item> itemList = new ArrayList<Item>();

		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		// getting JSON string from URL

		JSONObject json = jParser.makeHttpRequest(url_all_items, "GET", params);

		// Check your log cat for JSON reponse
		Log.d("All items check: ", json.toString());

		try {
			// Checking for SUCCESS TAG
			int success = json.getInt(TAG_SUCCESS);

			if (success == 1) {
				// items found
				// Getting Array of items
				items = json.getJSONArray(TAG_ITEM);
				// looping through All items
				for (int i = 0; i < items.length(); i++) {
					JSONObject c = items.getJSONObject(i);
					// Storing each json item in variable
					String id = c.getString(TAG_ITEMID);
					String name = c.getString(TAG_NAME);
					String description = c.getString(TAG_DESCRIPTION);
					String aisle = c.getString(TAG_AISLE);
					String x = c.getString(TAG_X);
					String y = c.getString(TAG_Y);
					String price = c.getString(TAG_PRICE);
					String quantity = c.getString(TAG_QUANTITY);
					String catid = c.getString(TAG_CATEGORYID);
					String storeid = c.getString(TAG_STOREID);
					Item temp = new Item(name, description, aisle, (int)Integer.parseInt(x), (int)Integer.parseInt(y), (double)Double.parseDouble(price), (int)Integer.parseInt(quantity), (int)Integer.parseInt(id), (int)Integer.parseInt(catid), (int)Integer.parseInt(storeid), new ArrayList<Integer>());
					itemList.add(temp);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return itemList;
	}
	
	public static int add(String name, String description, String aisle, int x, int y,
			double price, int quantity, int categoryId, int storeId){

		JSONParser jsonParser = new JSONParser();

		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("name", name));
		params.add(new BasicNameValuePair("description", description));
		params.add(new BasicNameValuePair("aisle", aisle));
		params.add(new BasicNameValuePair("x", Integer.toString(x)));
		params.add(new BasicNameValuePair("y", Integer.toString(y)));
		params.add(new BasicNameValuePair("price", Double.toString(price)));
		params.add(new BasicNameValuePair("quantity", Integer.toString(quantity)));
		params.add(new BasicNameValuePair("categoryid", Integer.toString(categoryId)));
		params.add(new BasicNameValuePair("storeid", Integer.toString(storeId)));

		
		// getting JSON Object
		// Note that create product url accepts POST method
		JSONObject json = jsonParser.makeHttpRequest(url_create_item,
				"POST", params);

		// check log cat fro response
		Log.d("Create Response", json.toString());

		// check for success tag
		try {
			int success = json.getInt(TAG_SUCCESS);

			if (success == 1) {
				return 1;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return 0;
	}
}
