package com.team.XMarkets;

import android.os.Parcel;
import android.os.Parcelable;

public class Promotion implements Parcelable
{
		private String name;
		private int id;
		private int storeId;
		private String description;
		private String startDate;
		private String endDate;
		private int cid;	
	
	public Promotion(String name, String desc, String stdate, String edate, int id, int cid, int s_id) 
	{
		this.name = name;
		this.id = id;
		this.cid = cid;
		this.storeId= s_id;
		this.description = desc;
		this.startDate = stdate;
		this.endDate=edate;	
	}
	
	//only for parceling
	public Promotion(Parcel source) {
		name = source.readString();
		id = source.readInt();
		storeId = source.readInt();
		description = source.readString();
		startDate = source.readString();
		endDate = source.readString();
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public int getstoreId() {
		return storeId;
	}
	
	public void setstoreId(int storeId) {
		this.storeId = id;
	}
	
	public String getDescription() {
		return description;
	}


	public void setDescription(String d) {
		this.description = d;
	}
	
	public String getstartDate() {
		return startDate;
	}


	public void setstartDate(String startDate) {
		this.startDate = startDate;
	}
	
	public String getendDate() {
		return endDate;
	}


	public void setendDate(String endDate) {
		this.endDate = endDate;
	}
	
	public String toString()
	{
		return name;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		
		dest.writeString(name);
		dest.writeInt(id);
		dest.writeInt(storeId);
		dest.writeString(description);
		dest.writeString(startDate);
		dest.writeString(endDate);
	}
	
	 public static final Parcelable.Creator<Promotion> CREATOR = new Parcelable.Creator<Promotion>() {
	      public Promotion createFromParcel(Parcel source) {
	            return new Promotion(source);
	      }
	      public Promotion[] newArray(int size) {
	            return new Promotion[size];
	      }
	};
	
	

}


