package com.team.XMarkets;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class CatagoryData {
	// url to create new category
	private static String url_create_category = "http://192.168.1.18/xmarkets/create_category.php";
	private static final String url_update_category = "http://192.168.1.18/xmarkets/update_category.php";
	private static final String url_delete_category = "http://192.168.1.18/xmarkets/delete_category.php";
	private static String url_all_categories = "http://192.168.1.18/xmarkets/get_all_categories.php";

	private static final String TAG_SUCCESS = "success";
	private static final String TAG_CATEGORYID = "categoryid";
	private static final String TAG_NAME = "name";
	private static final String TAG_STOREID = "storeid";
	private static final String TAG_CATEGORY = "category";

	public static int update(String name, int categoryId, int storeId){

		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(TAG_NAME, name));
		params.add(new BasicNameValuePair(TAG_CATEGORYID, Integer.toString(categoryId)));
		params.add(new BasicNameValuePair(TAG_STOREID, Integer.toString(storeId)));

		JSONParser jsonParser = new JSONParser();

		// sending modified data through http request
		// Notice that update category url accepts POST method
		JSONObject json = jsonParser.makeHttpRequest(url_update_category,
				"POST", params);

		// check json success tag
		try {
			int success = json.getInt(TAG_SUCCESS);

			if (success == 1) {
				return 1;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return 0;
	}

	public static int remove(int categoryId){
		JSONParser jsonParser = new JSONParser();

		int success;
		try {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("categoryid", Integer.toString(categoryId)));

			// getting category details by making HTTP request
			JSONObject json = jsonParser.makeHttpRequest(
					url_delete_category, "POST", params);

			// check your log for json response
			Log.d("Delete category", json.toString());

			// json success tag
			success = json.getInt(TAG_SUCCESS);
			if (success == 1) {
				return 1;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return 0;
	}

	public static ArrayList<Category> get(){
		
		JSONParser jParser = new JSONParser();
		JSONArray categorys = null;
		ArrayList<Category> categoryList = new ArrayList<Category>();

		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		// getting JSON string from URL

		JSONObject json = jParser.makeHttpRequest(url_all_categories, "GET", params);

		// Check your log cat for JSON reponse
		Log.d("All categorys check: ", json.toString());

		try {
			// Checking for SUCCESS TAG
			int success = json.getInt(TAG_SUCCESS);

			if (success == 1) {
				// categorys found
				// Getting Array of categorys
				categorys = json.getJSONArray(TAG_CATEGORY);
				// looping through All categorys
				for (int i = 0; i < categorys.length(); i++) {
					JSONObject c = categorys.getJSONObject(i);
					// Storing each json category in variable
					String name = c.getString(TAG_NAME);
					String catid = c.getString(TAG_CATEGORYID);
					String storeid = c.getString(TAG_STOREID);
					Category temp = new Category(name, (int)Integer.parseInt(catid), (int)Integer.parseInt(storeid), null, null);
					categoryList.add(temp);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return categoryList;
	}
	
	public static int add(String name, int storeId){

		JSONParser jsonParser = new JSONParser();

		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("name", name));
		params.add(new BasicNameValuePair("storeid", Integer.toString(storeId)));

		
		// getting JSON Object
		// Note that create category url accepts POST method
		JSONObject json = jsonParser.makeHttpRequest(url_create_category,
				"POST", params);

		// check log cat fro response
		Log.d("Create Response", json.toString());

		// check for success tag
		try {
			int success = json.getInt(TAG_SUCCESS);

			if (success == 1) {
				return 1;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return 0;
	}
}