package com.team.XMarkets;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class ItemInfo extends Activity {

	private Item item;
	private TextView name;
	private TextView price;
	private TextView description;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.iteminfo);
		
		item = getIntent().getExtras().getParcelable("item");
		setWidgets();

	}

	private void setWidgets() {
		name = (TextView) findViewById(R.id.itemInfo_name);
		name.setText(item.getName());
		
		price = (TextView) findViewById(R.id.itemInfo_price);
		price.setText(((Double)item.getPrice()).toString());
		
		description = (TextView) findViewById(R.id.itemInfo_description);
		description.setText(item.getDescription());
	}

}
