package com.team.XMarkets;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Canvas;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.ImageView;

public class StoreMap extends Activity {

	private ImageView storeMap;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.storemap);

		setWidgets();
	}


	private void setWidgets() {
		storeMap = (ImageView) findViewById(R.id.store_map);
		storeMap.setImageResource(R.drawable.store_map);
		
		Bitmap bitmap = Bitmap.createBitmap(storeMap.getDrawable().getIntrinsicWidth(), 
				storeMap.getDrawable().getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(bitmap);
		Paint paint = new Paint();
		paint.setColor(Color.RED);
		
		ArrayList<Item> items = ((Utilities)getApplication()).getUserItems();
		for(int i = 0; i < items.size(); i++)
		{
			int x = items.get(i).getXCoordinate();
			int y = items.get(i).getYCoordinate();
			canvas.drawCircle(x, y, 5, paint);
		}

		
		ImageView itemLocations = new ImageView(this);
		itemLocations.setImageBitmap(bitmap);
		FrameLayout layout = (FrameLayout) findViewById(R.id.StoreMapLayout);
		layout.addView(itemLocations);
		setContentView(layout);

	}


}
