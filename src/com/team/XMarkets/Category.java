package com.team.XMarkets;

import java.util.ArrayList;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;
public class Category implements Parcelable
{
	    private String name; 
		private int id;
		private int storeId;
		private ArrayList<Integer> collectionpromotionId = new ArrayList<Integer>();
		private List<Item> collectionItems = new ArrayList<Item>();
		
	public Category(String c_name, int id, int s_id, ArrayList<Integer> p_id, ArrayList<Item> items)
	{
		name = c_name;
		this.id = id;
		this.storeId = s_id;
		this.collectionpromotionId = p_id;
		this.collectionItems = items;
	}
	
	//only for parceling
	public Category(Parcel source) {
		name = source.readString();
		id = source.readInt();
		storeId = source.readInt();
		source.readList(collectionpromotionId,Integer.class.getClassLoader());
		source.readTypedList(collectionItems, Item.CREATOR);
	}
	
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public int getStoreId() {
		return storeId;
	}
	
	public void setStoreId(int storeId) {
		this.storeId = id;
	}
	
	public ArrayList<Integer> getPromotionIds() {
		return collectionpromotionId;
	}
	
	public void setPromotionIds(ArrayList<Integer> ids) {
		this.collectionpromotionId = ids;
	}
	
	public void addToPromotionIds(Integer a){
		collectionpromotionId.add(a);
	}
	public void removeFromPromotionItems(Integer b){
		collectionpromotionId.remove(b);
	}
	public List<Item> getCollectionItems() {
		return collectionItems;
	}
	public void setCollectionItems(List<Item> items) {
		this.collectionItems = items;
	}
	public void addToCollectionItems(Item a){
		collectionItems.add(a);
	}
	public void removeFromCollectionItems(Item b){
		 collectionItems.remove(b);
	}
	public String toString()
	{
		return name;	
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		
		dest.writeString(name);
		dest.writeInt(id);
		dest.writeInt(storeId);
		dest.writeList(collectionpromotionId);
		dest.writeTypedList(collectionItems);
	}
	
	 public static final Parcelable.Creator<Category> CREATOR = new Parcelable.Creator<Category>() {
	      public Category createFromParcel(Parcel source) {
	            return new Category(source);
	      }
	      public Category[] newArray(int size) {
	            return new Category[size];
	      }
	};
	
	

}


