package com.team.XMarkets;
import java.util.ArrayList;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

public class Store implements Parcelable{

	private String name;
	private String address;
	private String email; 
	private String phone;
	private int id;

	private String username;
	private String password;

	private List<Category> collectionCategory = new ArrayList<Category>();
	private List<Promotion> collectionPromotion = new ArrayList<Promotion>();
	private List<Item> collectionItem = new ArrayList<Item>();

	public Store(String s_name, String address, String email, String ph, int id, String user, String pwd, ArrayList<Category> c, ArrayList<Promotion> p, ArrayList<Item> i)
	{
		this.name = s_name;
		this.address = address;
		this.email = email;
		this.phone = ph;
		this.id = id;
		this.username = user;
		this.password = pwd;
		this.collectionCategory = c;
		this.collectionPromotion = p; 
		this.collectionItem = i;

	}
	//only for parceling
	public Store(Parcel source) {
		name = source.readString();
		address = source.readString();
		email = source.readString();
		phone = source.readString();
		id = source.readInt();
		username = source.readString();
		password = source.readString();
		source.readTypedList(collectionCategory, Category.CREATOR);
		source.readTypedList(collectionPromotion, Promotion.CREATOR);
		source.readTypedList(collectionItem, Item.CREATOR);

	}
	public String getName() {
		return name;
	}



	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	public String toString()
	{
		return name;	
	}
	public void setCollectionCategory(List<Category> c) {
		this.collectionCategory = c;
	}

	public List<Category> getCollectionCategory() {
		return collectionCategory;
	}
	public void setCollectionItem(List<Item> i) {
		this.collectionItem = i;
	}

	public List<Item> getCollectionItem() {
		return collectionItem;
	}
	public void setCollectionPromotion(List<Promotion> p) {
		this.collectionPromotion = p;
	}

	public List<Promotion> getCollectionPromotion() {
		return collectionPromotion;
	}
	public void addToCollectionItems(Item a){
		collectionItem.add(a);
	}
	public void removeFromCollectionItems(Item b){
		collectionItem.remove(b);
	}
	public void addToCollectionCategory(Category a){
		collectionCategory.add(a);
	}
	public void removeFromCollectionCategory(Category b){
		collectionCategory.remove(b);
	}
	public void addToCollectionPromotion(Promotion a){
		collectionPromotion.add(a);
	}
	public void removeFromCollectionPromotion(Promotion b){
		collectionPromotion.remove(b);
	}
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public void writeToParcel(Parcel dest, int flags) {

		dest.writeString(name);
		dest.writeString(address);
		dest.writeString(email);
		dest.writeString(phone);
		dest.writeInt(id);
		dest.writeString(username);
		dest.writeString(password);
		dest.writeTypedList(collectionCategory);
		dest.writeTypedList(collectionPromotion);
		dest.writeTypedList(collectionItem);
	}
	public static final Parcelable.Creator<Store> CREATOR = new Parcelable.Creator<Store>() {
		public Store createFromParcel(Parcel source) {
			return new Store(source);
		}
		public Store[] newArray(int size) 
		{
			return new Store[size];
		}
	};
}