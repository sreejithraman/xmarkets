package com.team.XMarkets;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

//implements parcelable to pass through intents
public class Item implements Parcelable
{
	private String name;
	private String description;
	private String aisle; 
	private int xcoordinate;
	private int ycoordinate;

	private int id;
	private int categoryId;
	private int storeId;
	private ArrayList<Integer> promotionIds = new ArrayList<Integer>();

	private double price; 
	private int quantity;
		
	public Item(String name, String desc, String aisle, int xcoordinate,int ycoordinate,
			double price, int qty, int id, int c_id, int s_id, ArrayList<Integer> p_id) 
	{
		this.name = name;
		description = desc;
		this.aisle = aisle;
		this.xcoordinate = xcoordinate;
		this.ycoordinate = ycoordinate;
		this.id = id;
		this.price = price;
		quantity = qty;
		categoryId = c_id;
		storeId = s_id;
		promotionIds = p_id;
	}
	
	//only for parceling
	public Item(Parcel source) {
		name = source.readString();
		description = source.readString();
		aisle = source.readString();
		xcoordinate = source.readInt();
		ycoordinate = source.readInt(); 
		id = source.readInt();
		categoryId = source.readInt();
		storeId = source.readInt();
		source.readList(promotionIds,Integer.class.getClassLoader());
		price = source.readDouble();
		quantity = source.readInt();
		
		
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getAisle() {
		return aisle;
	}


	public void setAisle(String aisle) {
		this.aisle = aisle;
	}

	public int getXCoordinate() {
		return xcoordinate;
	}


	public void setXCoordinate(int xcoordinate) {
		this.xcoordinate = xcoordinate;
	}
	public int getYCoordinate() {
		return ycoordinate;
	}


	public void setYCoordinate(int ycoordinate) {
		this.ycoordinate = ycoordinate;
	}



	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getCategoryId() {
		return categoryId;
	}


	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}


	public ArrayList<Integer> getPromotionIds() {
		return promotionIds;
	}


	public void setPromotionIds(ArrayList<Integer> promotionIds) {
		this.promotionIds = promotionIds;
	}


	public int getStoreId() {
		return storeId;
	}


	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}


	public double getPrice() {
		return price;
	}


	public void setPrice(int price) {
		this.price = price;
	}


	public int getQuantity() {
		return quantity;
	}


	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}


	public String toString()
	{
		return name;
		
	}


	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(name);
		dest.writeString(description);
		dest.writeString(aisle);
		dest.writeInt(xcoordinate);
		dest.writeInt(ycoordinate);
		dest.writeInt(id);
		dest.writeInt(categoryId);
		dest.writeInt(storeId);
		dest.writeList(promotionIds);
		dest.writeDouble(price);
		dest.writeInt(quantity);
	}
	
	 public static final Parcelable.Creator<Item> CREATOR = new Parcelable.Creator<Item>() {
	      public Item createFromParcel(Parcel source) {
	            return new Item(source);
	      }
	      public Item[] newArray(int size) {
	            return new Item[size];
	      }
	};
	
	

}
